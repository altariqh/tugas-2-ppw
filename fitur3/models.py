from django.db import models

# Create your models here.
class Forum(models.Model):
    #title = models.CharField(max_length=80)
    date = models.DateTimeField(auto_now_add=True)
    content = models.TextField(max_length=1000)
    company_id = models.IntegerField(default=0)

    class Meta:
       ordering = ['id']

class Comment(models.Model):
    name    = models.CharField(max_length=140)
    #title   = models.CharField(max_length=140)
    content = models.CharField(max_length=500)
    forum   = models.ForeignKey(Forum)
    created_on = models.DateTimeField(auto_now_add=True)