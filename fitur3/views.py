from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.
response = {}

def index(request):
    forum_list = Forum.objects.filter().order_by('-date')
    paginator = Paginator(forum_list, 2) # Show 2 posts per page

    page = request.GET.get('page')
    try:
        forum = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        forum = paginator.page(1)
    response['forum'] = forum
    return render(request, 'forum.html', response)

def add_forum(request):
    if request.method == 'POST':

        title = forms.CharField(max_length=140)
        content = forms.CharField(max_length=500)

        response['title'] = title
        response['content'] = content

        Forum.objects.create(title=request.POST['title'], content=request.POST['content'])
        
        forum = Forum.objects.all()
        response['forum'] = forum
        return redirect('/forum/')

def delete_forum(request, id):
    delete_forum = Forum.objects.get(id=id)
    delete_forum.delete()
    return redirect('/forum/')