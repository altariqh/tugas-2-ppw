"""masterdir URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from django.views.generic.base import RedirectView
import fitur1.urls as fitur1
import fitur2.urls as fitur2
import fitur3.urls as fitur3

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^fitur2/', include(fitur2,namespace='fitur2')),
    url(r'^fitur3/', include(fitur3,namespace='fitur3')),    
    url(r'^$',RedirectView.as_view(url='/fitur1/', permanent=True), name='index'),
]