from django.conf.urls import url
from .views import request_token, index, request_auth
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^authenticate/', request_auth, name='auth'),
    url(r'^request-token/', request_token , name='auth'),
]