from django.shortcuts import render, redirect
from django.http import HttpRequest, JsonResponse, HttpResponseRedirect
from fitur3.models import Forum, Comment
# from fitur3.forms import ForumForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import  json

CLIENT_ID = "866hc1so0n9js9"
CLIENT_SECRET = "IFSny4QeaB0XYT8D"
REDIRECT_URI = "http://127.0.0.1:8000/login/request-token/"

# Create your views here.
response = {}
def index(request):
    forum_list = Forum.objects.all()
    paginator = Paginator(forum_list, 3)

    page = request.GET.get('page')
    try:
        forum = paginator.page(page)
    except:
        forum = paginator.page(1)

    response['forum'] = forum
    return render(request, 'test.html', response)

def request_auth(request: HttpRequest):
    uri = ("https://www.linkedin.com/oauth/v2/authorization?" +
           "response_type=code&" +
           "client_id=" + CLIENT_ID + "&" +
           "redirect_uri=" + REDIRECT_URI + "&" +
           "state=987654321&" +
           "scope=r_basicprofile rw_company_admin")


    return redirect(uri)

def request_token(request: HttpRequest):
    request_token_uri = "https://www.linkedin.com/oauth/v2/accessToken"


    if request.method == "GET":
        code = request.GET['code']
        state = request.GET['state']
        request_data = {
            "grant_type":"authorization_code",
            "code":code,
            "redirect_uri":REDIRECT_URI,
            "client_id":CLIENT_ID,
            "client_secret":CLIENT_SECRET
        }

        print(">>>>>>>>>>>>>  CODE <<<<<<<<<<<<<")
        print(code)

        request_header = {
            'content_type':"application/json"
        }
        response = requests.request("POST", request_token_uri, data=request_data,
                         headers=request_header)
        response_data = json.loads(response.content.decode('utf8'))
        print(">>>>>>>>>>>>>  TOKEN <<<<<<<<<<<<<")
        print(response_data['access_token'])
        request.session['session-id'] = response_data['access_token']

        get_company_data_url = "https://api.linkedin.com/v1/people/~?format=json&oauth2_access_token="+request.session['session-id']
        test_request = requests.get(get_company_data_url)
        print(test_request.content.decode('utf8'))

        #  return HttpResponseRedirect('/profile/', reques)
        return redirect('/profile/')
    else:
        return redirect('/')

